const multer = require('multer');

// Configuración del almacenamiento de las fotos
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/'); // Ruta donde se almacenarán las fotos (crea una carpeta "uploads" en el directorio raíz de tu proyecto)
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1E9)}`; // Genera un nombre de archivo único
    const extension = file.originalname.split('.').pop(); // Obtiene la extensión del archivo original
    cb(null, `auto-${uniqueSuffix}.${extension}`); 
  }
});

// Configuración de Multer
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5 // Tamaño máximo de archivo: 5 MB
  },
  fileFilter: (req, file, cb) => {
    // Filtrar solo imágenes
    if (file.mimetype.startsWith('image/')) {
      cb(null, true);
    } else {
      cb(new Error('El archivo debe ser una imagen válida.'));
    }
  }
});

module.exports = upload;
