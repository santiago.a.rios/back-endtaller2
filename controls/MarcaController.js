'use strict';

const { validationResult } = require('express-validator');
const { marca } = require('../models');

class MarcaController {
  async listar(req, res) {
    try {
      const lista = await marca.findAll({
        attributes: ['nombre']
      });
      if (lista.length > 0) {
        res.status(200).json({ msg: 'OK!', code: 200, info: lista });
      } else {
        res.status(200).json({ msg: 'Lista vacía', code: 200, info: [] });
      }
    } catch (error) {
      res.status(500).json({ msg: 'Error al obtener la lista de marcas', code: 500 });
    }
  }
}

module.exports = MarcaController;