'use strict';

const { validationResult } = require('express-validator');
var models = require('../models/');
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8;
let jwt = require('jsonwebtoken');
class CuentaController {
    async sesion(req, res) {
        let errors = validationResult(req);
        if (error.isEmpty()) {
            var login = await cuenta.findOne({
                where: { correo: req.body.email },
                include: { model: models.persona, as: "persona", attributes: ['apellidos'] }
            });
            if (login === null) {
                res.status(400);
                res.json({ msg: "Cuenta no encontrada", code: 400 })
            } else {
                res.status(200);
                var isClaveValida = function (clave, claveUser) {
                    return bcrypt.compareSync(claveUser, clave);
                };
                if (login.estado) {
                    if (isClaveValida(login.clave, req.body.clave)) {
                        const tokenData = {
                            external: login.external_id,
                            email: login.correo,
                            check: true
                        };
                        require('dotenv').config();
                        const llave = process.env.KEY;
                        const token = jwt.sign(tokenData, llave, {
                            expiresIn: '12h'
                        });
                        res.json({
                            msg: "OK!",
                            token: token,
                            user: login.persona.nombres + '' + login.persona.apellidos,
                            correo: login.correo,
                            code: "200"
                        })
                    } else{
                        res.json({ msg: "Clave Incorrecta!", code: 200});
                    }
                }
            }
        }
    }
}
module.exports = CuentaController;