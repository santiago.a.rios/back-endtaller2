'use strict';

const { validationResult } = require('express-validator');
const { factura, auto, usuario } = require('../models');

class FacturaController {
  async listar(req, res) {
    try {
      const lista = await factura.findAll({
        include: [
          { model: auto, as: 'auto', attributes: ['modelo', 'color', 'external_id', 'anio', 'precio', 'motor', 'tipo_combustible', 'estado'] },
          { model: usuario, as: 'usuario', attributes: ['nombre', 'email'] }
        ],
        attributes: ['fecha', 'monto']
      });
      if (lista.length > 0) {
        res.status(200).json({ msg: 'OK!', code: 200, info: lista });
      } else {
        res.status(200).json({ msg: 'Lista vacía', code: 200, info: [] });
      }
    } catch (error) {
      res.status(500).json({ msg: 'Error al obtener la lista de facturas', code: 500 });
    }
  }

  async obtener(req, res) {
    const id = req.params.id;
    try {
      const facturaData = await factura.findOne({
        where: { id },
        include: [
          { model: auto, as: 'auto', attributes: ['modelo', 'color', 'external_id', 'anio', 'precio', 'motor', 'tipo_combustible', 'estado'] },
          { model: usuario, as: 'usuario', attributes: ['nombre', 'email'] }
        ],
        attributes: ['fecha', 'monto']
      });
      if (facturaData) {
        res.status(200).json({ msg: 'OK!', code: 200, info: facturaData });
      } else {
        res.status(404).json({ msg: 'Factura no encontrada', code: 404 });
      }
    } catch (error) {
      res.status(500).json({ msg: 'Error al obtener la factura', code: 500 });
    }
  }

  async guardar(req, res) {
    const { auto_id, usuario_id, fecha, monto } = req.body;

    if (!auto_id || !usuario_id || !fecha || !monto) {
      return res.status(400).json({ msg: 'Faltan datos', code: 400 });
    }

    try {
      const autoData = await auto.findOne({ where: { id: auto_id } });
      const usuarioData = await usuario.findOne({ where: { id: usuario_id } });

      if (!autoData || !usuarioData) {
        return res.status(400).json({ msg: 'Datos no encontrados', code: 400 });
      }

      const facturaData = {
        auto_id,
        persona_id,
        fecha,
        monto
      };

      const newFactura = await factura.create(facturaData);

      res.status(200).json({ msg: 'Factura creada exitosamente', code: 200, info: newFactura });
    } catch (error) {
      res.status(500).json({ msg: 'Error al crear la factura', code: 500 });
    }
  }
}

module.exports = FacturaController;
