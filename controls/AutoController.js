'use strict';

const { validationResult } = require('express-validator');
const { auto, marca } = require('../models');

class AutoController {
  async listar(req, res) {
    try {
      const lista = await auto.findAll({
        include: { model: marca, as: 'marca', attributes: ['nombre_marca'] },
        attributes: ['modelo', 'color', 'external_id', 'anio', 'precio', 'motor', 'tipo_combustible', 'estado']
      });
      res.status(200).json({ msg: 'OK!', code: 200, info: lista });
    } catch (error) {
      res.status(500).json({ msg: 'Error al obtener la lista de autos', code: 500 });
    }
  }

  async obtener(req, res) {
    try {
      const external = req.params.external;
      const lista = await auto.findOne({
        where: { external_id: external },
        include: { model: marca },
        attributes: ['modelo', 'color', 'external_id', 'anio', 'precio', 'motor', 'tipo_combustible', 'estado']
      });
      if (lista === null) {
        res.status(404).json({ msg: 'Auto no encontrado', code: 404 });
      } else {
        res.status(200).json({ msg: 'OK!', code: 200, info: lista });
      }
    } catch (error) {
      res.status(500).json({ msg: 'Error al obtener el auto', code: 500 });
    }
  }

  async guardar(req, res) {
    try {
      const errors = validationResult(req);
      if (errors.isEmpty()) {
        const marca_id = req.body.external_marca;
        if (marca_id) {
          const marcaAux = await marca.findOne({ where: { external_id: marca_id } });
          if (marcaAux) {
            const data = {
              modelo: req.body.modelo,
              color: req.body.color,
              anio: req.body.anio,
              precio: req.body.precio,
              motor: req.body.motor,
              tipo_combustible: req.body.combustible,
              marcaId: marcaAux.id
            };
            const autoCreado = await auto.create(data);
            res.status(200).json({ msg: 'Se han registrado sus datos', code: 200, info: autoCreado });
          } else {
            res.status(404).json({ msg: 'Marca no encontrada', code: 404 });
          }
        } else {
          res.status(400).json({ msg: 'Faltan datos', code: 400 });
        }
      } else {
        res.status(400).json({ msg: 'Datos faltantes', code: 400, errors: errors.array() });
      }
    } catch (error) {
      res.status(500).json({ msg: 'Error al guardar el auto', code: 500 });
    }
  }
}

module.exports = AutoController;