var express = require('express');
var router = express.Router();
const { body } = require('express-validator');
const RolController = require('../controls/RolController');
const PersonaController = require('../controls/PersonaController');
const CuentaController = require('../controls/CuentaController');
var rolController = new RolController();
var personaController = new PersonaController();
var cuentaController = new CuentaController();
let jwt = require('jsonwebtoken');
const upload = require('../fotos/multerConfig');

//middleware
var auth = function middleware(req, res, next) {
  const token = req.headers['x-api-token'];
  //console.log(token);
  //console.log(req.headers);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log(err);
        res.status(401);
        res.json({ msg: "Token no valido o expirado", code: 401 });
      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({
          where: { external_id: req.decoded.external }
        });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  //res.send('respond with a resource');
  res.json({ "version": "1.0", "name": "auto" });
});

//CUENTA
router.post('/sesion', [
  body('email', 'Ingrese un correo valido!').trim().exists().not().isEmpty(),
  body('clave', 'Ingrese la clave').trim().exists().not().isEmpty()
], cuentaController.sesion);
//FIN CUENTA

router.get('/roles', rolController.listar);

//persona
router.post('/personas/guardar', [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 100 "),
  body('nombres', 'Ingrese algun dato').trim().exists()
], personaController.guardar);
router.post('/personas/modificar', personaController.modificar);
router.get('/personas/listar', auth, personaController.listar);
router.get('/persona/obtener/:external', auth, personaController.obtener);

//fotos
router.post('/autos/foto', upload.single('foto'), (req, res) => {
  res.json({ success: true, message: 'Foto de auto subida correctamente.' });
});

/*
router.get('/suma/:a/:b', function (req, res, next) {
  var a = req.params.a * 1;
  var b = Number(req.params.b);
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });
});

router.post('/sumar', function (req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if (isNaN(a) || isNaN(b)) {
    res.status(400);
    res.json({ "msg": "FALTAN DATOS"});
  }
  console.log(b);
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });
});
*/
module.exports = router;
