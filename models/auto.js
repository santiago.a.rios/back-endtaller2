'use strict';

module.exports = (sequelize, DataTypes) => {
    const auto = sequelize.define('auto', {
        modelo: { type: DataTypes.STRING(50), defaultValue: "NO_DATA" },
        color: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        anio: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        motor: { type: DataTypes.ENUM("GASOLINA", "DIESEL", "ELECTRICO", "HIBRIDO"), unique: true, defaultValue: "GASOLINA" },
        precio: { type: DataTypes.STRING(50), defaultValue: "NO_DATA" },
        tipo_combustible: { type: DataTypes.ENUM("ECO", "SUPER", "DIESEL"), unique: true, defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    auto.associate = function (models) {
        auto.belongsTo(models.persona, { foreignKey: 'id_persona' });
        auto.belongsTo(models.marca, { foreignKey: 'id_marca' });
    };

    return auto;
};