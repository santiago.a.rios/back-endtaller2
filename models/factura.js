'use strict';

module.exports = (sequelize, DataTypes) => {
  const factura = sequelize.define('factura', {
    nombre_empresa: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
    nro_factura: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
    telefono: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
    direccion: { type: DataTypes.STRING(255), allowNull: true, defaultValue: "NO_DATA" },
    fecha: { type: DataTypes.DATE, allowNull: false },
    external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
  }, { freezeTableName: true });

  factura.associate = function (models) {
    factura.belongsTo(models.auto, { foreignKey: 'auto_id', as: 'auto' });
    factura.belongsTo(models.persona, { foreignKey: 'persona_id', as: 'persona' });
  };

  return factura;
};
