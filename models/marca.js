'use strict';// todos los codigos deben estar estrictamente códificados

module.exports = (sequelize, DataTypes) => {
    const marca = sequelize.define('marca', {
        nombre_marca: { type: DataTypes.STRING(20) },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    marca.associate = function (models) {
        marca.hasMany(models.auto, { foreignKey: 'id_marca', as: 'auto' });
    };

    return marca;
};