'use strict';

module.exports = (sequelize, DataTypes) => {
    const detalleFactura = sequelize.define('detallefactura', {
        nombre_empresa: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        nro_factura: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        fecha: { type: DataTypes.STRING(20), unique: true, allowNull: false, defaultValue: "NO_DATA" }, //unique campo unico
        telefono: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        direccion: { type: DataTypes.STRING(255), allowNull: true, defaultValue: "NO_DATA" },
        monto: { type: DataTypes.DECIMAL(10, 2), allowNull: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    persona.associate = function (models) {
        detalleFactura.belongsTo(models.rol, { foreignKey: 'id_rol' });
        detalleFactura.hasOne(models.cuenta, { foreignKey: 'id_persona', as: 'cuenta' });
    };

    return detalleFactura;
};