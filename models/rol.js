'use strict';// todos los codigos deben estar estrictamente códificados

module.exports = (sequelize, DataTypes) => {
    const rol = sequelize.define('rol', {
        nombre: { type: DataTypes.STRING(20) },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });

    rol.associate = function (models) {
        rol.hasMany(models.persona, { foreignKey: 'id_rol', as: 'persona' });
    };

    return rol;
};